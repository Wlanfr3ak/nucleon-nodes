# Nucleon Tracking Node

Dieser Node ist für das Tracking mittel TTN Tracker App gebaut.
Die einzige zusätzliche Funktion ist die Übermittlung der Akkuspannung um einen Trigger auszulösen wenn der Akku leer wird.

## Port 1 Node

Ein Port 1 Node ist ein einfacher Basis Node der zum Tracken mittels eines Handys genutzt werden kann.

Es sind keine besonderen Anpassungen an der Programmierung notwendig, wenn man mit dem vorgegebenen Sketch arbeitet. Zur Vervollständigung wird hier aber natürlich auch die Schnittstelle definiert.
Bedingung der Payload Schnittstelle ist:

Port 1 wird genutzt

Akkustand   //in mV

Payload Codierung im Sketch:

        int batt = (int)(readVcc() / 100);  // readVCC returns  mVolt need just 100mVolt steps
        byte batvalue = (byte)batt; // no problem putting it into a int.

        unsigned char mydata[6];
        mydata[0] = batvalue;      

Übertragung der Daten beim Senden

    LMIC_setTxData2($Port, $Payload, sizeof($Payload), 0);
    LMIC_setTxData2(1, (uint8_t*) mydata, sizeof(mydata), 0);
    
Payload Decodierung in der Applikation:

    function Decoder(b, port) {
        if (port == 2) {
        var batt = (b[0]/10.0)
        }
    }
    
Den Trigger kann man auch in der Payloadfunktion auslösen.