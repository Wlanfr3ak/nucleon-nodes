/*****************************************************
 * TTN Config
 *****************************************************/
#define OTAA 1
#if !defined(OTAA)   
/***************************************************** 
 *  ABP 
 *****************************************************/
static const PROGMEM u1_t NWKSKEY[16] = {"aus TTN Console holen"}; // LoRaWAN NwkSKey, network session key 
static const u1_t PROGMEM APPSKEY[16] = {"aus TTN Console holen"}; // LoRaWAN AppSKey, application session key 
static const u4_t DEVADDR = 0x000000 ; // LoRaWAN end-device address (DevAddr)
/****************************************************
 * OTAA
 * 
 * Diese callbacks werden nur für die over-the-air activation verwendet
 * hier leer gelassen (wir können sie nicht vollständig auslassen, es sei denn
 * DISABLE_JOIN ist in config.h eingestellt, sonst beschwert sich der Linker).
 ****************************************************/
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }
/*********************************************************/
#else
/*******************************************************************************
 * OTAA Teil
 *******************************************************************************/
/*******************************************************************************
 * Diese EUI muss im Little-Endian-Format vorliegen, also das niederwertigste Byte
 * zuerst. Beim Kopieren eines EUI aus der Ausgabe von ttnctl bedeutet dies das Umkehren
 * die Bytes Für von TTN ausgegebene EUIs sollten die letzten Bytes 0xD5, 0xB3 sein.
 * 0x70.
 *******************************************************************************/
static const u1_t PROGMEM DEVEUI[8]={"aus TTN Console holen"};
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
/*******************************************************************************
 * Dies sollte auch im Little-Endian-Format erfolgen, siehe oben.
 *******************************************************************************/
static const u1_t PROGMEM APPEUI[8]={"aus TTN Console holen"};
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
/*******************************************************************************
 * Dieser Schlüssel sollte im Big Endian-Format vorliegen (oder, da er nicht wirklich ein
 * Nummer, aber ein Speicherblock, Endianness trifft nicht wirklich zu. Im
 * In der Praxis kann ein aus ttnctl entnommener Schlüssel so wie er ist kopiert werden.
 *******************************************************************************/
static const u1_t PROGMEM APPKEY[16] = {"aus TTN Console holen"};
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}
#endif

 
