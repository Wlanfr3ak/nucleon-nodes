# Nucleon Nodes

Die Nodes der Workshops im November 2018

Dieses Repo kann herunter geladen werden und das Verzeichnis dann als Sketchbook Verzeichnis in den Einstellungen der Arduino IDE genutzt werden.

Im Verzeichnis Tracking findet ihr die Tracking Node Typen und

Im Verzeichnis Weather die Nodes, um Umwelt Messwerte zu erfassen

Wir erweitern diese Umgebung nach und nach so, dass wir eine bunte Übersicht aller möglichen Nodes hier abbilden.


Die Nodes können alle an die Nucleon Applicationen des passenden Node Typ reporten und so zentral aufbereitet werden.

Die aktuellen Sketche sind nun OTAA fähig.
